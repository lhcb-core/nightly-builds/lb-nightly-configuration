###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ruff: noqa: E501
import os
from pathlib import Path
from tempfile import TemporaryDirectory

import pytest
import yaml


class MockFunc:
    """
    Helper class to record the arguments a callback is called with.
    """

    def __init__(self):
        self.args = None
        self.kwargs = None
        self.__name__ = "mock"

    def __call__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


def test_Project():
    from lb.nightly.configuration import Project

    mockCheckout = MockFunc()

    p = Project("Gaudi", "v23r5", checkout=mockCheckout)
    assert p.name == "Gaudi"
    assert p.version == "v23r5"
    assert p.overrides == {}
    assert p.checkout == mockCheckout

    p = Project(
        "Gaudi",
        "v23r5",
        overrides={
            "GaudiPolicy": "head",
            "GaudiProfiling": None,
            "GaudiObjDesc": "v11r17",
        },
        checkout=mockCheckout,
    )
    assert p.name == "Gaudi"
    assert p.version == "v23r5"
    assert p.overrides == {
        "GaudiPolicy": "head",
        "GaudiProfiling": None,
        "GaudiObjDesc": "v11r17",
    }
    assert p.checkout == mockCheckout

    cb = MockFunc()
    p = Project("Gaudi", "v23r5", checkout=(cb, {"special": False}))
    assert p.checkout == cb
    assert p.checkout_opts == {"special": False}


def test_artifacts_checkout():
    from lb.nightly.configuration import DBASE, Package, Project, Slot

    p = Project("AProject", "v1r0")

    assert (
        p.artifacts("checkout")
        == "checkout/AProject/6e/6eb38d626371d5e5c124e879e10767894b67742a7d0e3a4441586a0dabce2e8e.zip"
    )

    assert (
        Project("AProject", "master", checkout_opts={"commit": "abc123"}).artifacts(
            "checkout"
        )
        == "checkout/AProject/6c/6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090.zip"
    )

    assert (
        Project("AProject", "master").artifacts("checkout")
        == "checkout/AProject/75/75d2b73d9825068d10f980219892b4308f0253d3eb3d8b490913c26c29f9803c.zip"
    )

    assert (
        Project(
            "AProject",
            "master",
            checkout_opts={
                "commit": "abc123",
                "merges": [(1, "commit_1"), (2, "commit_2")],
            },
        ).artifacts("checkout")
        == "checkout/AProject/2c/2c5bc0be3fdbc9aa4810c23b75f4609cdc6f9ae0d20a8c52e18c4456993da4be.zip"
    )

    assert (
        Project(
            "AProject",
            "master",
            checkout_opts={
                "merges": [(1, "commit_1"), (2, "commit_2")],
            },
        ).artifacts("checkout")
        == "checkout/AProject/17/17bf44217bc01ec04056bc2aa37dc490ff30d0ad7c0625c98c466ba9e1b9e46e.zip"
    )

    assert (
        Project(
            "AProject",
            "master",
            checkout_opts={
                "commit": "abc123",
                "merges": [(2, "commit_2"), (1, "commit_1")],
            },
        ).artifacts("checkout")
        == "checkout/AProject/cf/cf267b943f9babedae9ecef346c2a3714480c2b16afecc0a657d2c87c41dea55.zip"
    )

    assert (
        Project(
            "AProject",
            "master",
            checkout_opts={
                "commit": "abc000",
                "merges": [(1, "commit_1"), (2, "commit_2")],
            },
        ).artifacts("checkout")
        == "checkout/AProject/5a/5a876f4dbd7b7ef5ea114b40a8c715257f91adf4c3bf3def32f41d4b0dbb2380.zip"
    )

    assert (
        Project(
            "AProject",
            "master",
            checkout_opts={
                "commit": "abc123",
                "merges": [(1, "commit_1"), (999, "commit_2")],
            },
        ).artifacts("checkout")
        == "checkout/AProject/2c/2c5bc0be3fdbc9aa4810c23b75f4609cdc6f9ae0d20a8c52e18c4456993da4be.zip"
    )

    assert (
        Project(
            "AProject",
            "master",
            checkout_opts={
                "commit": "abc123",
                "merges": [(999, "commit_2")],
            },
        ).artifacts("checkout")
        == "checkout/AProject/25/25aeee81477ef394905fbaad097f942f9c455cd65c3409b8fde93f7bd1ddf696.zip"
    )

    _ = Slot("ASlot", build_id=10, projects=[p])

    assert (
        p.artifacts("checkout")
        == "checkout/AProject/3c/3c1069237081a77885d9ea8e5b4d0d0194289a2b61129fbfb274c39afbfb8b8d.zip"
    )

    p = Package("APackage", "v1r0")

    assert (
        p.artifacts("checkout")
        == "checkout/APackage/30/30ce010f9b8961a08e8887aa67b62c9766293885aaf8d680c2d64a12c423e7ff.zip"
    )

    p = DBASE(
        packages=[
            Package(
                "PRConfig",
                "HEAD",
                checkout_opts={
                    "commit": "abc123",
                },
            )
        ]
    )

    assert (
        p.packages[0].artifacts("checkout")
        == "checkout/PRConfig/69/693c146a5ef7920572a6133b932c2c366e9a036f3f10755baa0984ee5368dd52.zip"
    )

    p = DBASE(
        packages=[
            Package(
                "PRConfig",
                "master",
                checkout_opts={
                    "commit": "abc123",
                },
            )
        ]
    )

    assert (
        p.packages[0].artifacts("checkout")
        == "checkout/PRConfig/ed/ed26e667df5303c7a4b0534d522e6c2d208a07ee9b5fbf04331b87c72a10a153.zip"
    )


def test_artifacts():
    from lb.nightly.configuration import Project, Slot

    p = Project("AProject", "v1r0")

    assert (
        p.artifacts("build", "x86_64-centos7-gcc9-opt")
        == "packs/x86_64-centos7-gcc9-opt/AProject.v1r0.x86_64-centos7-gcc9-opt.zip"
    )

    assert (
        p.artifacts("test", "x86_64-centos7-gcc9-opt")
        == "tests/x86_64-centos7-gcc9-opt/AProject.zip"
    )

    _ = Slot("ASlot", build_id=10, projects=[p])

    assert (
        p.artifacts("build", "x86_64-centos7-gcc9-opt")
        == "nightly/ASlot/10/packs/x86_64-centos7-gcc9-opt/AProject.v1r0.ASlot.10.x86_64-centos7-gcc9-opt.zip"
    )

    assert (
        p.artifacts("test", "x86_64-centos7-gcc9-opt")
        == "nightly/ASlot/10/tests/x86_64-centos7-gcc9-opt/AProject.zip"
    )


def test_artifacts_bad_args():
    from lb.nightly.configuration import DBASE, Package, Project, Slot

    p = Project("AProject", "v1r0")

    for args in [
        ("checkout", "x86_64-centos7-gcc9-opt"),
        ("build",),
        ("test",),
        ("stage", "x86_64-centos7-gcc9-opt"),
        ("stage",),
    ]:
        with pytest.raises(ValueError):
            p.artifacts(*args)

    _ = Slot("ASlot", build_id=10, projects=[p])

    for args in [
        ("checkout", "x86_64-centos7-gcc9-opt"),
        ("build",),
        ("test",),
        ("stage", "x86_64-centos7-gcc9-opt"),
        ("stage",),
    ]:
        with pytest.raises(ValueError):
            p.artifacts(*args)

    pkg = Package("APackage", "v1r0")
    dp = DBASE(packages=[pkg])
    for args in [
        ("checkout", "x86_64-centos7-gcc9-opt"),
        ("build",),
        ("build", "x86_64-centos7-gcc9-opt"),
        ("test",),
        ("test", "x86_64-centos7-gcc9-opt"),
        ("stage", "x86_64-centos7-gcc9-opt"),
        ("stage",),
    ]:
        with pytest.raises(ValueError):
            pkg.artifacts(*args)
        with pytest.raises(ValueError):
            dp.artifacts(*args)


def test_id():
    from lb.nightly.configuration import DBASE, Package, Project, Slot

    p = Project("AProject", "v1r0")
    assert p.id() == "AProject/v1r0"

    pkg = Package("APackage", "v1r0")
    assert pkg.id() == "APackage/v1r0"
    pkg2 = Package("WG/Config", "v1r0")
    assert pkg2.id() == "WG/Config/v1r0"

    dp = DBASE(packages=[pkg, pkg2])
    assert dp.id() == "DBASE"

    s = Slot("test", build_id=10, projects=[p, dp])
    assert s.AProject.id() == "nightly/test/10/AProject"
    assert dp.id() == "nightly/test/10/DBASE"
    assert pkg.id() == "nightly/test/10/DBASE/APackage"
    assert pkg.id(show_container=False) == "nightly/test/10/APackage"
    assert pkg2.id() == "nightly/test/10/DBASE/WG/Config"
    assert pkg2.id(show_container=False) == "nightly/test/10/WG/Config"


def test_Package():
    from lb.nightly.configuration import Package

    mockCheckout = MockFunc()
    p = Package("Gen/DecFiles", "Sim10", checkout=mockCheckout)
    assert p.name == "Gen/DecFiles"
    assert p.version == "Sim10"
    assert p.checkout == mockCheckout

    cb = MockFunc()
    p = Package("Gen/DecFiles", "Sim10", checkout=(cb, {"special": False}))
    assert p.checkout == cb
    assert p.checkout_opts == {"special": False}


def test_deployment_dir():
    from lb.nightly.configuration import DBASE, Package, Project, Slot

    project = Project("Gaudi", "master")
    assert isinstance(project.get_deployment_directory(), Path)
    assert (
        str(project.get_deployment_directory())
        == "/cvmfs/lhcbdev.cern.ch/nightlies/Gaudi/master"
    )

    dbase = DBASE(packages=[Package("PRConfig", "HEAD")])
    for pkg in dbase.packages:
        assert (
            str(pkg.get_deployment_directory())
            == "/cvmfs/lhcbdev.cern.ch/nightlies/DBASE/PRConfig"
        )

    _ = Slot("myslot", projects=[project, dbase])
    assert (
        str(project.get_deployment_directory())
        == "/cvmfs/lhcbdev.cern.ch/nightlies/nightly/myslot/0/Gaudi"
    )
    assert (
        str(pkg.get_deployment_directory())
        == "/cvmfs/lhcbdev.cern.ch/nightlies/nightly/myslot/0/DBASE/PRConfig"
    )

    with TemporaryDirectory() as tmpdirname:
        os.environ["PRIVATE_DIR"] = tmpdirname
        with open(os.path.join(tmpdirname, "secrets.yaml"), "w") as fp:
            yaml.dump({"installations": {"path": "/my/path/to/cache"}}, fp)
            assert (
                str(project.get_deployment_directory())
                == "/my/path/to/cache/nightly/myslot/0/Gaudi"
            )
