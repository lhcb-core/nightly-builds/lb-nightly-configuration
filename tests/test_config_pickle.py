###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Uncomment to disable the tests.
# __test__ = False

import pickle

from lb.nightly.configuration import DBASE, Package, Project, Slot


def test_project():
    orig = Project(
        "Gaudi",
        "v26r1",
        checkout="git",
        checkout_opts=dict(url="http://git.cern.ch/pub/gaudi"),
        build_method="cmake",
        env=["USE_MAKE=1"],
    )
    clone = pickle.loads(pickle.dumps(orig))
    assert orig.toDict() == clone.toDict()


def test_package():
    orig = Package("AppConfig", "head")
    clone = pickle.loads(pickle.dumps(orig))
    assert orig.toDict() == clone.toDict()


def test_slot():
    orig = Slot(
        "test",
        [
            Project(
                "Gaudi",
                "v26r1",
                checkout="git",
                checkout_opts=dict(url="http://git.cern.ch/pub/gaudi"),
                build_tool="cmake",
                env=["USE_MAKE=1"],
            ),
            DBASE(packages=[Package("AppConfig", "head")]),
        ],
        build_tool="cmt",
        env=["Hello=World"],
    )
    clone = pickle.loads(pickle.dumps(orig))
    assert orig.toDict() == clone.toDict()


def test_slot2():
    orig = Slot(
        "test",
        [
            DBASE(packages=[Package("AppConfig", "head")]),
            Project(
                "Gaudi",
                "v26r1",
                checkout="git",
                checkout_opts=dict(url="http://git.cern.ch/pub/gaudi"),
                build_tool="cmake",
                env=["USE_MAKE=1"],
            ),
        ],
        build_tool="cmt",
        env=["Hello=World"],
    )
    clone = pickle.loads(pickle.dumps(orig))
    assert orig.toDict() == clone.toDict()
