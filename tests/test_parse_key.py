###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def test_KeyTuple():
    from lb.nightly.configuration import _parse_key

    cases = {
        "nightly/lhcb-gaudi-head/1234/Gaudi": (  #
            "nightly",
            "lhcb-gaudi-head",
            1234,
            "Gaudi",
        ),
        "nightly/lhcb-gaudi-head/1234": ("nightly", "lhcb-gaudi-head", 1234, None),  #
        "lhcb-gaudi-head/1234": ("nightly", "lhcb-gaudi-head", 1234, None),  #
        "testing/lhcb-gaudi-head/1234/Gaudi": (  #
            "testing",
            "lhcb-gaudi-head",
            1234,
            "Gaudi",
        ),
        "lhcb-head/5432/LHCb": ("nightly", "lhcb-head", 5432, "LHCb"),  #
        "test-slot/Project": ("nightly", "test-slot", 0, "Project"),  #
        "dummy/test-slot/0": ("dummy", "test-slot", 0, None),  #
        "just-slot": ("nightly", "just-slot", 0, None),  #
        "flav/slot/proj": ("flav", "slot", 0, "proj"),  #
        "nightly/myslot/123/DBASE/PRConfig": (
            "nightly",
            "myslot",
            123,
            "DBASE/PRConfig",
        ),
        "nightly/myslot/123/DBASE/WG/CharmConfig": (
            "nightly",
            "myslot",
            123,
            "DBASE/WG/CharmConfig",
        ),
    }
    for key, expected in list(cases.items()):
        # check that we get what we expect
        parsed = _parse_key(key)
        assert parsed == expected
        # and make sure that conversion to string is stable
        assert _parse_key(str(parsed)) == expected

    try:
        _parse_key("a/b/c/d")
        assert False, "ValueError expected"
    except ValueError:
        pass
