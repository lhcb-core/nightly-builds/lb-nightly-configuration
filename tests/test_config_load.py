###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration import Slot


def add_defaults(expected):
    """
    Helper to avoid hardcoding everywhere default values.
    """
    slot_defaults = {
        "packages": [],
        "description": "Generic nightly build slot.",
        "build_tool": "cmake",
        "build_id": 0,
        "disabled": False,
        "env": [],
        "platforms": [],
        "preconditions": [],
        "error_exceptions": [],
        "warning_exceptions": [],
    }
    project_defaults = {
        "checkout": "default",
        "checkout_opts": {},
        "disabled": False,
        "env": [],
        "overrides": {},
        "dependencies": [],
        "with_shared": False,
    }
    package_defaults = {"checkout": "default", "container": "DBASE"}

    for k in slot_defaults:
        if k not in expected:
            expected[k] = slot_defaults[k]

    for project in expected["projects"]:
        for k in project_defaults:
            if k not in project:
                project[k] = project_defaults[k]

    for package in expected.get("packages", []):
        for k in package_defaults:
            if k not in package:
                package[k] = package_defaults[k]


def test_loadJSON_3():
    "JSON with data packages"
    data = {
        "slot": "slot-with-packages",
        "packages": [
            {"checkout_opts": {"export": True}, "name": "ProdConf", "version": "v1r19"},
            {
                "checkout_opts": {"export": True},
                "container": "PARAM",
                "name": "TMVAWeights",
                "version": "v1r4",
            },
        ],
        "projects": [],
    }
    expected = dict(data)
    add_defaults(expected)
    expected["projects"] = [
        {
            "checkout": "ignore",
            "disabled": False,
            "name": "DBASE",
            "no_test": True,
            "platform_independent": True,
            "version": "None",
        },
        {
            "checkout": "ignore",
            "disabled": False,
            "name": "PARAM",
            "no_test": True,
            "platform_independent": True,
            "version": "None",
        },
    ]

    slot = Slot.fromDict(data)
    found = slot.toDict()

    # order of projects and packages is not relevant in this case
    found["projects"].sort(key=lambda x: x["name"], reverse=False)
    found["packages"].sort(key=lambda x: x["name"], reverse=False)
    expected["projects"].sort(key=lambda x: x["name"], reverse=False)
    expected["packages"].sort(key=lambda x: x["name"], reverse=False)

    assert found == expected
