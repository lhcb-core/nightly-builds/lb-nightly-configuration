###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from lb.nightly.configuration.utils import sortedByDeps


def test_sortedByDeps():
    elements = sortedByDeps(
        {"4": ["2", "3"], "3": ["1"], "2": ["1"], "1": ["0"], "0": []}
    )
    # 0 and 1 must be at the beginning...
    assert elements[:2] == ["0", "1"]
    # ... 4 at the end...
    assert elements[-1] == "4"
    # ... no requirement on the order of 2 and 3
    assert sorted(elements[2:-1]) == ["2", "3"]

    # check that the order of keys is preserved when possible
    # (note that in Python 3 dictionaries preserve the order of insertion)
    assert sortedByDeps(dict([("1", []), ("3", ["1"]), ("2", ["1"])])) == [
        "1",
        "3",
        "2",
    ]
