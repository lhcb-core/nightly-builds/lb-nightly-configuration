###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from unittest.mock import patch

import pytest

from lb.nightly.configuration import (
    DataProject,
    Package,
    Project,
    Slot,
    get,
)


def test_invalid_get_argument():
    with pytest.raises(ValueError):
        get("test-slot")


@patch(
    "lb.nightly.configuration.json.load",
    **{
        "return_value": {
            "config": {
                "slot": "my-test-slot",
                "description": "Generic nightly build slot.",
                "projects": [
                    {
                        "name": "Gaudi",
                        "version": "master",
                    }
                ],
            }
        }
    },
)
@patch("lb.nightly.configuration.urlopen", **{"return_value.returncode": 200})
@patch(
    "lb.nightly.configuration.service_config",
    **{"return_value": {"couchdb": {"url": "http://user:pass@asd/db"}}},
)
def test_find_from_couchdb(conf, urlmock, jl):
    slot = get("my-test-slot/1")
    conf.assert_called_once()
    urlmock.assert_called_once()
    jl.assert_called_once()
    assert isinstance(slot, Slot)
    assert slot.name == "my-test-slot"
    assert len(slot.projects) == 1


@patch(
    "lb.nightly.configuration.service_config",
    **{"return_value": {"couchdb": {"url": "http://user:pass@asd/db"}}},
)
def test_find_failed(conf):
    from urllib.error import URLError

    with pytest.raises(URLError):
        get("my-slot/1")


@patch(
    "lb.nightly.configuration.json.load",
    **{
        "return_value": {
            "config": {
                "slot": "my-test-slot",
                "description": "Generic nightly build slot.",
                "projects": [
                    {"name": "Gaudi", "version": "master"},
                    {"name": "DBASE"},
                    {"name": "PARAM"},
                ],
                "packages": [
                    {"name": "PRConfig", "version": "head", "container": "DBASE"},
                    {"name": "ParamFiles", "version": "head", "container": "PARAM"},
                    {"name": "WG/CharmConfig", "version": "head", "container": "DBASE"},
                ],
            }
        }
    },
)
@patch("lb.nightly.configuration.urlopen", **{"return_value.returncode": 200})
@patch(
    "lb.nightly.configuration.service_config",
    **{"return_value": {"couchdb": {"url": "http://user:pass@asd/db"}}},
)
def test_get_project(conf, urlmock, jl):
    project = get("nightly/my-test-slot/1/Gaudi")
    conf.assert_called_once()
    urlmock.assert_called_once()
    jl.assert_called_once()
    assert isinstance(project, Project)
    assert project.name == "Gaudi"
    assert project.version == "master"
    package = get("nightly/my-test-slot/1/DBASE/PRConfig")
    assert isinstance(package, Package)
    assert package.name == "PRConfig"
    assert package.version == "head"
    package = get("nightly/my-test-slot/1/PARAM/ParamFiles")
    assert isinstance(package, Package)
    assert package.name == "ParamFiles"
    assert package.version == "head"
    package = get("nightly/my-test-slot/1/DBASE/WG/CharmConfig")
    assert isinstance(package, Package)
    assert package.name == "WG/CharmConfig"
    assert package.version == "head"
    dp = get("nightly/my-test-slot/1/DBASE")
    assert isinstance(dp, DataProject)
    assert dp.name == "DBASE"
    with pytest.raises(KeyError):
        get("nightly/my-test-slot/1/DBASE/PRconfig")
    with pytest.raises(KeyError):
        get("nightly/my-test-slot/1/gaudi")
