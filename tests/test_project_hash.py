###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ruff: noqa: E501
from copy import deepcopy
from pathlib import Path
from typing import Mapping

import pytest

from lb.nightly.configuration import DataProject, Slot

# Allow making copies of a slot without hidden shares
Slot.clone = lambda self: self.fromDict(deepcopy(self.toDict()))


@pytest.fixture
def slot() -> Slot:
    import json

    return Slot.fromDict(
        json.load(open(Path(__file__).parent / "testing.lhcb_head.2243.config.json"))
    )


def get_hashes(slot, platform="") -> Mapping[str, str]:
    return {p.name: p.hash(platform) for p in slot.projects}


def test_source_hashes(slot):
    expected = {
        "Alignment": "8787503edf784c6d0e2e3ce11866492f61d5a56a33bf60866111d54f4e50f933",
        "AlignmentOnline": "5dc031739d59548a2c709747cd6317c269a9826e11468b53ab0debd9f9c044a8",
        "Allen": "68a6f5e133c7d053077e589a6d1db9321f9b067e65f52c91c274125079f1a235",
        "Analysis": "98eb60b063b0933ab688be44e104ac0b7e01abe6b95aed9d9c1f348a8bb6fd5c",
        "Boole": "740136785ade0166b800eaddeed875b9dc58825a80a9da03f37d1343ecf261ea",
        "DaVinci": "0df6705dbf374651f807a4b159c290a2d5f800a4eb128fca3516df796f941e82",
        "DBASE": "f7debd7c359b70034309920880337e3d4206e743ad588329d60393487874ff23",
        "Detector": "2729f4d352de1eaf2e05995554a08e1df1a0d5e3a21e5d72d431f6b21c96a6df",
        "Gaudi": "6e56ce3e395a59a23852e323d178067e7fa32958ad88b364fed223fe17c92632",
        "Gauss": "873131926a9ec4d39d4ba01a72bd0ad2f909b54de76853a3a84a5157923b3cca",
        "Geant4": "70f82147271d20afe8e7077e9e2082d04b0bba6393eef8e33fdcca7d85162453",
        "Kepler": "fed3677cc8045029ef0342eb5ad58ae5f61f8f34a266bacc6527acf7b4fed7e2",
        "Lbcom": "a57aa75b79b6fbd2c88da43e276670052d3ab468ef4282c0ff17504506a2f070",
        "LCG": "0b1e45eecc4feaaa3b7a255d9aa0eca525240498fbdcdc78fabf4102f94749c6",
        "LHCb": "6cef8ef2addc5a9a48ba709f5cf82e3fa706e9ec176ccc0fdf0ee006fc020712",
        "LHCbIntegrationTests": "7589507c65e74e967bbef7ed0db53c9b988f5061808b17463ac434d31d2056a4",
        "Moore": "0cb5d65a3e337b88594ab9309c833f980f73117f79c794517d5767576cd65e6e",
        "MooreAnalysis": "4b05f44c36e7d29f8abfc06499f4e68320136fb5f85516db334e556acfcb992b",
        "MooreOnline": "18ba5c0cef70acd2c58a29f452bc87e8016db5cf2d02a0f318f49dfbc5928818",
        "Noether": "3a4ce8f8f133a773ad4a782684ff4a5942c0ae45721400a6d176f8c997ee6a01",
        "Online": "29d35782a6a696ce9eb469f6b1d5d5ad91c301fd8ef62b2173d3402cbb6e5991",
        "Panoptes": "26eb6cd3f0d297bc9164ca4e14e5a9f273c63826913427e455dac25f5114b34a",
        "Panoramix": "92389ff780c476101c14bcf1182960974f297163dd28c2935bf69b24c058ee11",
        "PARAM": "9789f01eff47084173cc3d85e7a7d2c7e770d40ae0195ce8d721f4597f9803f0",
        "Rec": "731fa311e93ba552f63b09b16361190b01ccdf37808983a1d52fcd26bb0bff43",
        "Run2Support": "99cca9de5d4da1429d037923ddaf39b7da553115f9e55bd61bf1977ee6b00e88",
    }
    assert get_hashes(slot) == expected


def test_binary_hashes(slot):
    expected = {
        "Alignment": "c53726cdce289331bf4f50770fb802434af90c0ec1435d74e01ae18a4f3caed0",
        "AlignmentOnline": "4988ab5fc4df638e7df54bcc742c9f7d8e616d3245d1956603e217dbbe8c9947",
        "Allen": "24931e30e5eab50f05bef03ae9f9f69aecc51bb038b6a406dbf34e98611c8369",
        "Analysis": "40fd61e946b482b19606ffab1ccf7637bc34e108f269a432ad2fd7d26a2ca350",
        "Boole": "dc3b6ac6e5e4d68b6a61c45f3a9a17cacb2583d6a53a26d203643db517eea054",
        "DaVinci": "7822f304a1341018a30ffce2fa83c1d6065298ce2436cc10bb105420a6f1c421",
        "DBASE": "f7debd7c359b70034309920880337e3d4206e743ad588329d60393487874ff23",
        "Detector": "461ef647e5464fb91568e143ff5aed884c21f66a0973a2cdb9cb3104d15ed47d",
        "Gaudi": "ffb67e798ef33a6321f606c51687149b3ec7aaec77f7dd28724a2d115510f0cb",
        "Gauss": "5e863846a6d26e767d8d2629f4708dfd6918ef34aef46c2abd8bc455fa830a57",
        "Geant4": "dbcc8d8cafc7adb42325c4e1635ce2c685a771fe8d8740b271d87d88f53f7275",
        "Kepler": "53d8a38fbe0c576231b163497e2a52768faef625a88025b3031f9d83dd901a45",
        "Lbcom": "f75d523ab7fab7f78a4f9abd1388d48295a35a3f5d8865d3184f65ff724bb53b",
        "LCG": "7668c4131e0ba234412a5e7250ded36ea6f5a723382a7905495be9b896461594",
        "LHCb": "775f7fc02e697b0730763023c5a9beb385a833fe313ae9aca2af7f49b4f1cd21",
        "LHCbIntegrationTests": "0359d3ab55557b5226b96e0e9b33e5d5e15feb6f0e5852cccaf2000a165ad939",
        "Moore": "980db26ee0de241a9c520246b8398d22eb473eeb98236f69f2e89e14d2949864",
        "MooreAnalysis": "7051e1ab114c05cff16e7fb0596d707dd9903547dfcb3f06cc73220e71900923",
        "MooreOnline": "fe3b5d90a51f26a813617bd8debd50842b5434e85cd826a01fe27a09d1cb24f8",
        "Noether": "468b169c7bd692765f3e13a033549ecaad201282f53f9a701e9a395bda663665",
        "Online": "dbb6d36723f96607a9b8c0c00a1f76745750631eb34b2270f499529eef04db9d",
        "Panoptes": "79b7705cd7d813e3b1b4e7577e7b2f39ba2032a6f8fd78ce7b1947e873027ba5",
        "Panoramix": "26cb023b07f5b1ba3fd4391a1f398b336b5984a3b9c044e6ea7af4e10750b55b",
        "PARAM": "9789f01eff47084173cc3d85e7a7d2c7e770d40ae0195ce8d721f4597f9803f0",
        "Rec": "5808382956706e3e9c4c2d98630efca0f8b11abcc57b1b2624b7d436c2983f2c",
        "Run2Support": "b869a4b2b9091e9d524686177f111b66553d47c8702b17401369463f412aa45d",
    }
    assert get_hashes(slot, "dummy-platform") == expected


@pytest.mark.parametrize(
    "platform",
    [
        "",
        "x86_64_v2-centos7-gcc11-opt",
        "x86_64_v2-centos7-gcc11-dbg",
        "x86_64_v2-centos7-clang12-opt",
        "x86_64_v2-centos7-clang12-dbg",
        "x86_64_v3-centos7-gcc11-opt+g",
        "x86_64_v4+vecwid256-centos7-gcc11-opt",
        "x86_64_v2-centos7-gcc11+dd4hep-opt",
    ],
)
def test_build_id_invariance(slot, platform):
    slot2 = slot.clone()
    slot2.build_id += 1
    assert get_hashes(slot, platform) == get_hashes(slot2, platform)


def test_platform_change(slot):
    for p in slot.projects:
        if isinstance(p, DataProject):
            assert p.hash("platform-a") == p.hash(
                "platform-b"
            ), f"{p.name} hashes should be the same for different platforms"
        else:
            assert p.hash("platform-a") != p.hash(
                "platform-b"
            ), f"{p.name} hashes should be different for different platforms"


def test_project_commit_change(slot):
    # a change in LHCb affects LHCb (checkout and binary) and Moore (binary only), but not Gaudi
    slot2 = slot.clone()
    slot2.LHCb.checkout_opts["commit"] = "abc123"

    assert slot.Gaudi.hash() == slot2.Gaudi.hash()
    assert slot.Gaudi.hash("some-platform") == slot2.Gaudi.hash("some-platform")

    assert slot.LHCb.hash() != slot2.LHCb.hash()
    assert slot.LHCb.hash("some-platform") != slot2.LHCb.hash("some-platform")

    assert slot.Moore.hash() == slot2.Moore.hash()
    assert slot.Moore.hash("some-platform") != slot2.Moore.hash("some-platform")


def test_package_commit_change(slot):
    # a change in a package affects LHCb (and downstream) builds
    slot2 = slot.clone()
    slot2.DBASE.packages[0].checkout_opts["commit"] = "abc123"

    assert slot.Gaudi.hash() == slot2.Gaudi.hash()
    assert slot.Gaudi.hash("some-platform") == slot2.Gaudi.hash("some-platform")

    assert slot.LHCb.hash() == slot2.LHCb.hash()
    assert slot.LHCb.hash("some-platform") != slot2.LHCb.hash("some-platform")

    assert slot.Moore.hash() == slot2.Moore.hash()
    assert slot.Moore.hash("some-platform") != slot2.Moore.hash("some-platform")
