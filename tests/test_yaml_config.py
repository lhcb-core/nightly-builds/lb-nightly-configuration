###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import tempfile
from unittest.mock import patch

import configurator
import pytest
import yaml

from lb.nightly.configuration import lbnightly_settings, service_config


def test_service_config_good():
    with tempfile.NamedTemporaryFile() as fp:
        fp.write(
            b"""
couchdb:
  url: https://lhcbsoft:pass@lhcb-couchdb.cern.ch/nightly-builds
rabbitmq:
  user: lhcb
  password: pass
gitlab:
  token: gitlabtoken
        """
        )
        fp.seek(0)

        conf = service_config(fp.name)

        assert (
            conf["couchdb"]["url"]
            == "https://lhcbsoft:pass@lhcb-couchdb.cern.ch/nightly-builds"
        )
        assert conf["rabbitmq"]["user"] == "lhcb"
        assert conf["rabbitmq"]["password"] == "pass"
        assert conf["gitlab"]["token"] == "gitlabtoken"


def test_service_config_bad():
    with tempfile.NamedTemporaryFile() as fp:
        fp.write(
            b"""
couchdb:
  url: https://lhcbsoft:pass@lhcb-couchdb.cern.ch/nightly-builds
rabbitmq:
  user: lhcb
  password: pass
][]
gitlab:
  token: gitlabtoken
        """
        )
        fp.seek(0)

        with pytest.raises(yaml.YAMLError):
            service_config(fp.name)


@patch(
    "configurator.Config.from_path", **{"return_value": configurator.config.Config()}
)
def test_lbnightly_settings_only_defaults(conf):
    defaults = {
        "couchdb": {"url": "http://localhost:8080/couchdb/nightly-builds"},
        "rabbitmq": {"url": "amqp://guest:guest@localhost:5672"},
        "mysql": {"url": "db+mysql://"},
        "artifacts": {
            "uri": "http://localhost:8081/repository/nightly_builds_artifacts/"
        },
        "logs": {"uri": "http://localhost:8081/repository/nightly_builds_logs/"},
        "elasticsearch": {"uri": "http://localhost:9200"},
        "installations": {"path": "/cvmfs/lhcbdev.cern.ch/nightlies"},
    }
    settings = lbnightly_settings().data
    assert settings == defaults


def test_lbnightly_settings_defaults_system():
    system = {
        "couchdb": {"url": "http://system:8080/couchdb/nightly-builds"},
        "rabbitmq": {"url": "amqp://guest:guest@localhost:5672"},
        "mysql": {"url": "db+mysql://"},
        "artifacts": {
            "uri": "http://localhost:8081/repository/nightly_builds_artifacts/"
        },
        "logs": {"uri": "http://localhost:8081/repository/nightly_builds_logs/"},
        "other": {"uri": "http://localhost:9300"},
    }
    with tempfile.TemporaryDirectory() as tmpdirname:
        os.environ["PRIVATE_DIR"] = tmpdirname
        with open(os.path.join(tmpdirname, "secrets.yaml"), "w") as fp:
            yaml.dump(system, fp)
        settings = lbnightly_settings().data
        assert settings == {
            "couchdb": {"url": "http://system:8080/couchdb/nightly-builds"},
            "rabbitmq": {"url": "amqp://guest:guest@localhost:5672"},
            "mysql": {"url": "db+mysql://"},
            "artifacts": {
                "uri": "http://localhost:8081/repository/nightly_builds_artifacts/"
            },
            "logs": {"uri": "http://localhost:8081/repository/nightly_builds_logs/"},
            "elasticsearch": {"uri": "http://localhost:9200"},
            "installations": {"path": "/cvmfs/lhcbdev.cern.ch/nightlies"},
            "other": {"uri": "http://localhost:9300"},
        }


def test_lbnightly_settings_user():
    system = {
        "couchdb": {"url": "http://system:8080/couchdb/nightly-builds"},
        "rabbitmq": {"url": "amqp://guest:guest@localhost:5672"},
        "mysql": {"url": "db+mysql://"},
        "artifacts": {
            "uri": "http://localhost:8081/repository/nightly_builds_artifacts/"
        },
        "logs": {"uri": "http://localhost:8081/repository/nightly_builds_logs/"},
        "other": {"uri": "http://localhost:9300"},
    }
    user = {
        "couchdb": {"url": "http://system:8080/couchdb/nightly-builds"},
        "rabbitmq": {"url": "amqp://user:guest@localhost:5672"},
        "mysql": {"url": "db+mysql://"},
        "artifacts": {
            "uri": "http://localhost:8081/repository/nightly_builds_artifacts/"
        },
        "logs": {"uri": "http://localhost:8081/repository/nightly_builds_logs/"},
        "another": {"uri": "http://localhost:9400"},
    }
    with tempfile.TemporaryDirectory() as tmpdirname:
        os.environ["PRIVATE_DIR"] = tmpdirname
        with open(os.path.join(tmpdirname, "secrets.yaml"), "w") as fp:
            yaml.dump(system, fp)
        with tempfile.TemporaryDirectory() as tmpdirname:
            user_file = os.path.join(tmpdirname, "secrets.yaml")
            with open(user_file, "w") as fp:
                yaml.dump(user, fp)
            settings = lbnightly_settings(secrets_yaml=user_file).data
            assert settings == {
                "couchdb": {"url": "http://system:8080/couchdb/nightly-builds"},
                "rabbitmq": {"url": "amqp://user:guest@localhost:5672"},
                "mysql": {"url": "db+mysql://"},
                "artifacts": {
                    "uri": "http://localhost:8081/repository/nightly_builds_artifacts/"
                },
                "logs": {
                    "uri": "http://localhost:8081/repository/nightly_builds_logs/"
                },
                "elasticsearch": {"uri": "http://localhost:9200"},
                "installations": {"path": "/cvmfs/lhcbdev.cern.ch/nightlies"},
                "other": {"uri": "http://localhost:9300"},
                "another": {"uri": "http://localhost:9400"},
            }


@patch(
    "configurator.Config.from_path", **{"return_value": configurator.config.Config()}
)
def test_lbnightly_settings_missing(conf):
    with pytest.raises(AttributeError):
        lbnightly_settings().mydb
    with pytest.raises(KeyError):
        lbnightly_settings()["mydb"]
