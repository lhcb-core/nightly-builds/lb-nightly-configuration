###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path
from tempfile import TemporaryDirectory

import pytest
import yaml

from lb.nightly.configuration import Project, Slot
from lb.nightly.configuration.project import ProjectsList


def test_Slot():
    s = Slot(name="lhcb-gaudi-head")
    assert len(s.projects) == 0
    assert s.name == "lhcb-gaudi-head"


def test_ProjectsList():
    slot = object()  # dummy object for checking
    pl = ProjectsList(slot)
    assert len(pl) == 0

    pl.insert(0, Project("a", "v1r0"))
    assert len(pl) == 1
    a = pl["a"]
    assert a == pl[0]
    assert a.name == "a"
    assert a.slot is slot

    pl.append(Project("b", "v2r0"))
    assert len(pl) == 2
    b = pl["b"]
    assert b == pl[1]
    assert b.name == "b"
    assert b.slot is slot

    del pl[0]
    assert len(pl) == 1
    assert a.slot is None

    pl.append(Project("a/b", "v3r0"))
    assert len(pl) == 2
    a_b = pl["a/b"]
    assert a_b == pl[1]
    assert a_b.name == "a/b"
    assert a_b == pl["a_b"]

    # works as iterable
    assert [p.name for p in pl] == ["b", "a/b"]


def test_slot_projects():
    slot = Slot("test", projects=[Project("a", "v1r0"), Project("b", "v2r0")])
    assert len(slot.projects) == 2
    a, b = slot.projects
    assert a.slot == b.slot == slot
    assert a == slot.a
    assert b == slot.b

    del slot.b
    assert len(slot.projects) == 1
    assert "a" in slot.projects
    assert "b" not in slot.projects
    assert b.slot is None

    with pytest.raises(AttributeError):
        slot.projects = []


def test_deps():
    # explicit dependencies
    slot = Slot(
        "test",
        projects=[
            Project("A", "v1r0", dependencies=["Zero"]),
            Project("b", "v2r0", dependencies=["c", "a"]),
        ],
    )
    # slot.checkout()
    assert slot.A.dependencies() == ["Zero"]
    assert slot.b.dependencies() == ["A", "c"]

    full_deps = slot.fullDependencies()
    expected = {"b": ["A", "c"], "A": ["Zero"]}
    assert full_deps == expected, full_deps

    deps = slot.dependencies()
    expected = {"A": [], "b": ["A"]}
    assert deps == expected, deps

    b = Project("b", "v2r0", dependencies=["c", "a"])
    assert b.dependencies() == ["a", "c"]


def test_slot_def_args():
    # test default arguments
    dummy = Slot("dummy")
    assert len(dummy.projects) == 0

    dummy = Slot("dummy", [Project("A", "v1r0")])
    assert len(dummy.projects) == 1
    assert dummy.A.version == "v1r0"


def test_env():
    slot = Slot(
        "test",
        projects=[Project("a", "v1r0", env=["proj=a"])],
        env=["slot=test", "proj=none"],
    )

    special = {"CMAKE_PREFIX_PATH": os.getcwd(), "CMTPROJECTPATH": os.getcwd()}

    def expected(d):
        """hack to extend the expected dictionary with search paths"""
        d.update(special)
        return d

    # extend CMake and CMT search paths
    initial = {"CMAKE_PREFIX_PATH": "/usr/local/bin:/usr/bin"}
    env = slot.environment(initial)
    assert env["CMAKE_PREFIX_PATH"] == os.pathsep.join(
        [os.getcwd(), initial["CMAKE_PREFIX_PATH"]]
    )
    assert env["CMTPROJECTPATH"] == os.getcwd()

    initial = {"CMTPROJECTPATH": "/usr/local/bin:/usr/bin"}
    env = slot.environment(initial)
    assert env["CMTPROJECTPATH"] == os.pathsep.join(
        [os.getcwd(), initial["CMTPROJECTPATH"]]
    )
    assert env["CMAKE_PREFIX_PATH"] == os.getcwd()

    # with dummy env
    initial = {}
    env = slot.environment(initial)
    assert env == expected({"slot": "test", "proj": "none"}), (
        env,
        expected({"slot": "test", "proj": "none"}),
    )
    assert initial == {}

    env = slot.a.environment(initial)
    assert env == expected({"slot": "test", "proj": "a"})
    assert initial == {}

    # with os.environ
    key = "USER"
    if key not in os.environ:
        os.environ[key] = "dummy"
    value = os.environ[key]

    initial = dict(os.environ)

    slot.env.append(f"me=${{{key}}}")

    env = slot.environment()
    assert env["slot"] == "test"
    assert env["proj"] == "none"
    assert env[key] == value
    assert env["me"] == value
    assert os.environ == initial

    env = slot.a.environment()
    assert env["slot"] == "test"
    assert env["proj"] == "a"
    assert env[key] == value
    assert env["me"] == value
    assert os.environ == initial

    # # derived class
    # class SpecialSlot(Slot):
    #     projects = []
    #     env = ['slot=test', 'proj=none']

    # slot = SpecialSlot('test')

    # env = slot.environment({})
    # assert env == expected({'slot': 'test', 'proj': 'none'})


#     slot.env.append('another=entry')
#     env = slot.environment({})
#     assert env == expected({
#         'slot': 'test',
#         'proj': 'none',
#         'another': 'entry'
#     })
#     # ensure that touching the instance 'env' attribute does not change the
#     # class
#     assert SpecialSlot.env == SpecialSlot.__env__ == ['slot=test', 'proj=none']

# # derived class
# class ExtendedSlot(SpecialSlot):
#     env = ['proj=dummy']

# slot = ExtendedSlot('test')

# env = slot.environment({})
# assert env == expected({'slot': 'test', 'proj': 'dummy'})


def test_slot_desc():
    slot = Slot("test")
    assert slot.desc == Slot.__doc__.strip()

    slot = Slot("test", desc="a test slot")
    assert slot.desc == "a test slot"

    class MyTest(Slot):
        """
        This is My Test.
        """

    slot = MyTest("test")
    assert slot.desc == "This is My Test."

    class NoDesc(Slot):
        pass

    slot = NoDesc("test")
    assert slot.desc == "<no description>"


def test_no_patch_flag():
    s = Slot("lhcb-release")
    assert s.no_patch is False

    d = s.toDict()
    assert d.get("no_patch", False) is False

    s = Slot.fromDict(d)
    assert s.no_patch is False

    s = Slot("lhcb-release", no_patch=True)
    assert s.no_patch is True

    d = s.toDict()
    assert d.get("no_patch", False) is True

    s = Slot.fromDict(d)
    assert s.no_patch is True

    try:
        from io import StringIO

        s.patch(StringIO())
        assert False, "ValueError expected"
    except ValueError:
        pass


def test_no_test_flag():
    s = Slot("lhcb-no-test")
    assert s.no_test is False

    d = s.toDict()
    assert d.get("no_test", False) is False

    s = Slot.fromDict(d)
    assert s.no_test is False

    s = Slot("lhcb-no-test", no_test=True)
    assert s.no_test is True

    d = s.toDict()
    assert d.get("no_test", False) is True

    s = Slot.fromDict(d)
    assert s.no_test is True

    p = Project("Gaudi", "HEAD")
    assert p.no_test is False

    d = p.toDict()
    assert d.get("no_test", False) is False

    s = Project.fromDict(d)
    assert s.no_test is False

    p = Project("Gaudi", "HEAD", no_test=True)
    assert p.no_test is True

    d = p.toDict()
    assert d.get("no_test", False) is True

    s = Project.fromDict(d)
    assert p.no_test is True

    # p = DBASE()
    # assert p.no_test is True


def test_cloning():
    from copy import deepcopy

    p = Project("Gaudi", "HEAD", env=["v1=1"])
    p1 = deepcopy(p)
    p1.env.append("v2=2")
    assert p.env == ["v1=1"]
    assert p1.env == ["v1=1", "v2=2"]


def test_metadata():
    s = Slot("lhcb-no-metadata")
    assert not s.metadata

    d = s.toDict()
    assert "metadata" not in d

    s = Slot.fromDict(d)
    assert not s.metadata

    s = Slot("lhcb-with-metadata", metadata={"key": "value"})
    assert s.metadata == {"key": "value"}

    d = s.toDict()
    assert d.get("metadata") == {"key": "value"}

    s = Slot.fromDict(d)
    assert s.metadata == {"key": "value"}

    s = Slot("lhcb-extend-metadata")
    assert not s.metadata
    s.metadata["key"] = "value"
    assert s.metadata == {"key": "value"}


def test_flavour():
    s = Slot("lhcb-slot")
    assert not s.metadata
    assert s.flavour == "nightly"
    assert not s.metadata
    s.flavour = "testing"
    assert s.flavour == "testing"
    assert s.metadata
    assert s.metadata.get("flavour") == "testing"


@pytest.mark.parametrize(
    "name,expected",
    [
        ("lhcb-something", "nightly"),
        ("something", "nightly"),
        ("test-slot", "testing"),
        ("lhcb-release", "release"),
        ("release", "release"),
    ],
)
def test_implicit_flavour(name, expected):
    s = Slot(name)
    assert not s.metadata
    assert s.flavour == expected


def test_deployment_dir():
    from lb.nightly.configuration import Project, Slot

    slot = Slot("myslot")
    assert isinstance(slot.get_deployment_directory(), Path)
    assert (
        str(slot.get_deployment_directory())
        == "/cvmfs/lhcbdev.cern.ch/nightlies/nightly/myslot/0"
    )

    project = Project("Gaudi", "master")
    slot.projects.append(project)
    assert (
        str(slot.get_deployment_directory())
        == "/cvmfs/lhcbdev.cern.ch/nightlies/nightly/myslot/0"
    )

    with TemporaryDirectory() as tmpdirname:
        os.environ["PRIVATE_DIR"] = tmpdirname
        with open(os.path.join(tmpdirname, "secrets.yaml"), "w") as fp:
            yaml.dump({"installations": {"path": "/my/path/to/cache"}}, fp)
            assert (
                str(slot.get_deployment_directory())
                == "/my/path/to/cache/nightly/myslot/0"
            )


def test_artifacts():
    from lb.nightly.configuration import Slot

    slot = Slot("myslot")
    assert (
        slot.artifacts(stage="deployment_dir") == "nightly/myslot/0/deployment_dir.zip"
    )

    with pytest.raises(ValueError):
        slot.artifacts(stage="unknown")

    with pytest.raises(ValueError):
        slot.artifacts(stage="deployment_dir", platform="something")


def test_fromDict_setstate_consistent():
    import pickle

    from lb.nightly.configuration import DBASE, Package, Slot

    slot = Slot(
        "myslot",
        projects=[
            DBASE(
                packages=[Package("PRConfig", "HEAD"), Package("Gen/Decfiles", "HEAD")]
            ),
            Project("Gaudi", "master"),
        ],
    )

    assert (
        Slot.fromDict(slot.toDict()).toDict()
        == pickle.loads(pickle.dumps(slot)).toDict()
    )


def test_unique():
    from lb.nightly.configuration import DBASE, Project, Slot

    p = Project("Gaudi", "master")

    with pytest.raises(ValueError) as exc:
        s = Slot("test", projects=[p, DBASE(), DBASE()])
    assert "There is already project DBASE added to the slot." in str(exc.value)

    with pytest.raises(ValueError) as exc:
        s = Slot("test", projects=[p, p, DBASE()])
    assert "There is already project Gaudi/master added to the slot." in str(exc.value)

    s = Slot("test", projects=[p, DBASE()])
    with pytest.raises(ValueError) as exc:
        s.projects.append(DBASE())
    assert "There is already project DBASE added to the slot." in str(exc.value)

    s = Slot("test", projects=[p])
    with pytest.raises(ValueError) as exc:
        s.projects.append(p)
    assert "There is already project Gaudi/master added to the slot." in str(exc.value)

    s = Slot("test", projects=[p, DBASE()])
    with pytest.raises(ValueError) as exc:
        s.projects.insert(0, DBASE())
    assert "There is already project DBASE added to the slot." in str(exc.value)

    s = Slot("test", projects=[p])
    with pytest.raises(ValueError) as exc:
        s.projects.insert(1, p)
    assert "There is already project Gaudi/master added to the slot." in str(exc.value)

    s = Slot("test", projects=[DBASE(), p])
    assert [p.name for p in s.projects] == ["DBASE", "Gaudi"]
